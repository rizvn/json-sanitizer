package com.rizvn.jsonsanitizer;

import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Riz
 */
public class JsonSanitizerTest {

  JsonSanitizer sut = new JsonSanitizer();

  @Test
  public void loadFile() throws Exception {
    JsonObject json = sut.sanitizeFile("src/test/resources/corpus/file.json");
    System.out.println(json.toString());
    Assert.assertNotNull(json);
  }

  @Test
  public void sanitizeFiles() throws Exception{
    sut.sanitizeFiles("src/test/resources/raw", "src/test/resources/sanitised");
  }

  @Test
  public void sanitizeFilesLive() throws Exception{
    sut.sanitizeFiles("/opt/Development/dataflows/corpus", "/opt/Development/dataflows/sanitised");
  }


}