package com.rizvn.jsonsanitizer;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Riz
 */
public class JsonSanitizer {

  Gson gson = new GsonBuilder().setPrettyPrinting().create();
  ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);


  /**
   * Reads json in v8 format from src, sanitizes and places converted version in dest directory
   * @param src
   * @param dest
   * @throws Exception
   */
  public void sanitizeFiles(String src, String dest){
    File srcDir = new File(src);
    File destDir = new File(dest);
    String[] fileNames = srcDir.list();

    for(String filename: fileNames){
      executor.execute(()->{
        try {
          JsonObject result = sanitizeFile(new File(srcDir, filename).getAbsolutePath());

          try (Writer writer = new BufferedWriter(new FileWriter(new File(destDir, filename)))) {
            gson.toJson(result, writer);
          }
        }
        catch (Exception ex){
          throw new IllegalStateException(ex);
        }
      });
    }

    //wait for all executors to complete
    try{
      executor.shutdown();
      executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
    }
    catch (Exception ex){
      throw new RuntimeException(ex);
    }
  }

  /**
   * Reads V8 json as string and sanitizes to standard json
   * @param jsonString
   * @return
   */
  public JsonObject sanitize(String jsonString)
  {
    JsonParser parser = new JsonParser();

    //enable lenient reading
    JsonReader reader = new JsonReader(new StringReader(jsonString));
    reader.setLenient(true);

    JsonObject root = parser.parse(reader).getAsJsonObject();

    JsonObject newJson = new JsonObject();

    newJson.add("header", processObjectTypes(root.getAsJsonObject("header")));
    newJson.add("trailer", processObjectTypes(root.getAsJsonObject("trailer")));

    if(root.has("groups")){
      processGroups(newJson, root.getAsJsonArray("groups"));
    }

    return newJson;
  }


  /**
   * Reads json from file path in v8 json format, returns a sanitized version
   * @param filePath
   * @return
   */
  public JsonObject sanitizeFile(String filePath) {
      try(InputStream is = new BufferedInputStream(new FileInputStream(filePath))) {
        String jsonStr = IOUtils.toString(is, StandardCharsets.UTF_8);
        return sanitize(jsonStr);
      }
      catch (Exception ex) {
        throw new IllegalStateException("Failed to parse: "+ filePath, ex);
      }
  }

  /**
   * Process groups within v8 json building key value pairs
   * @param parent
   * @param groups
   */
  public void processGroups(JsonObject parent, JsonArray groups){
    groups.forEach(group -> {
      JsonObject groupObject = group.getAsJsonObject();
      String groupName = groupObject.get("groupName").getAsString();
      Integer line = groupObject.get("line").getAsInt();
      Integer level = groupObject.get("level").getAsInt();

      JsonArray groupItems = groupObject.getAsJsonArray(groupName);

      JsonObject items = new JsonObject();
      items.addProperty("line", line);
      items.addProperty("level", level);
      processItems(groupItems, items);

      if(groupObject.has("groups")){
        processGroups(items, groupObject.get("groups").getAsJsonArray());
      }

      //group name already exists
      if(parent.has(groupName)){

        JsonElement el = parent.get(groupName);

        //and group type is array
        if(el.isJsonArray()){

          //then add the itemGroup to the array
          JsonArray arr = el.getAsJsonArray();
          arr.add(items);
        }
        else{
          //if group is not an array, make it one
          //add the item group to the new array
          JsonArray arr = new JsonArray();
          arr.add(parent.get(groupName));
          arr.add(items);

          //replace group with array
          parent.add(groupName, arr);
        }
      }
      else {
        parent.add(groupName, items);
      }


    });
  }

  /**
   * Parse json object where each value is a string and parse numbers where possible
   * @param jsonObject
   * @return
   */
  public JsonObject processObjectTypes(JsonObject jsonObject){
    JsonObject newJsonObject = new JsonObject();
    jsonObject.entrySet().forEach(entry -> {
      String value = entry.getValue().getAsString();
      if(NumberUtils.isNumber(value)){
        newJsonObject.addProperty(entry.getKey(), NumberUtils.createNumber(value));
      }
      else {
        newJsonObject.addProperty(entry.getKey(), value);
      }
    });
    return newJsonObject;
  }

  /**
   * Process items within a group
   * @param itemArray
   * @param itemObject
   * @return
   */
  public JsonObject processItems(JsonArray itemArray, JsonObject itemObject){
     for(JsonElement item: itemArray){
       JsonObject jsonObject = item.getAsJsonObject();
       String itemName = jsonObject.get("itemName").getAsString();
       String value = jsonObject.get(itemName).getAsString();

       //skip blank strings
       if(StringUtils.isBlank(value)) continue;

       //if item with name exists
       if(itemObject.has(itemName)){
         JsonElement el = itemObject.get(itemName);

         //if value is an array
         if(el.isJsonArray()){
           //get array
           JsonArray arr = el.getAsJsonArray();

           //add item to array
           if(NumberUtils.isNumber(value)){
             arr.add(NumberUtils.createNumber(value));
           }
           else {
             arr.add(value);
           }
           itemObject.add(itemName, arr);
         }
         else{
           //if isnt an array, turn it into one and add entry
           JsonArray arr  = new JsonArray();
           arr.add(itemObject.get(itemName));

           //add item to array
           if(NumberUtils.isNumber(value)){
             arr.add(NumberUtils.createNumber(value));
           }
           else {
             arr.add(value);
           }

           itemObject.add(itemName, arr);

         }
       }
       else{
         if(NumberUtils.isNumber(value)){
           itemObject.addProperty(itemName, NumberUtils.createNumber(value));
         }
         else {
           itemObject.addProperty(itemName, value);
         }
       }
     };
     return itemObject;
  }
}
